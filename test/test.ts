import { expect } from "chai";
import * as q from "../src/q";
import { flights } from "./flights";

const Q = new q.IdNode();

// Register all the airline specific constructs. After doing this,
// call chaining should just work.
function registerDSL(): any {
    q.ASTNode.register('origin', function (city: string) {
        return this.filter("origin", q.RelationalOperator.EqualTo, city);
    });

    q.ASTNode.register('destination', function (city: string) {
        return this.filter("destination", q.RelationalOperator.EqualTo, city);
    });

    q.ASTNode.register('oneStop', function (origin: string, destination: string) {
        return this.selfJoin("destination", "origin")
                    .filter("origin_1", q.RelationalOperator.EqualTo, origin)
                    .filter("destination_2", q.RelationalOperator.EqualTo, destination);
    });

    q.ASTNode.register('carrier', function (carrier: string) {
        return this.filter("carrier", q.RelationalOperator.EqualTo, carrier);
    });

    q.ASTNode.register('date', function (date: Date) {
        return this.filter("year", q.RelationalOperator.EqualTo, date.getUTCFullYear())
                    .filter("day", q.RelationalOperator.EqualTo, date.getUTCDay());
    });

    return new q.IdNode();
}

describe("Tests", async () => {
    it("filter queries with equals", async () => {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY");

        var executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(1221);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(1221);
    });

    it("filter queries with not equals", async () => {
        const query = Q.filter("origin", q.RelationalOperator.NotEqualTo, "New York NY");

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(8779);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(8779);
    });

    it("filter queries with greater than", async () => {
        const query = Q.filter("departureDelay", q.RelationalOperator.GreaterThan, 5.0);

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(3236);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(3236);
    });

    it("filter queries with greater than or equal to", async () => {
        const query = Q.filter("departureDelay", q.RelationalOperator.GreaterThanOrEqualTo, 5.0);

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(3388);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(3388);
    });

    it("filter queries with less than", async () => {
        const query = Q.filter("departureDelay", q.RelationalOperator.LessThan, 5.0);

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(6612);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(6612);
    });

    it("filter queries with less than or equal to", async () => {
        const query = Q.filter("departureDelay", q.RelationalOperator.LessThanOrEqualTo, 5.0);

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(6764);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(6764);
    });

    it("filter queries with select", async () => {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
                        .select("destination");

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(1221);
        expect(result[0].destination).to.equal("Los Angeles CA");

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(1221);
    });

    it("filter queries with select multiple", async () => {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
                        .select("destination", "carrier");

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(1221);
        expect(result[0].destination).to.equal("Los Angeles CA");
        expect(result[0].carrier).to.equal("American Airlines Inc.");

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(1221);
        expect(sqlResult[0].destination).to.equal("Los Angeles CA");
        expect(sqlResult[0].carrier).to.equal("American Airlines Inc.");

    });

    it("filter queries for source in Airline DSL", async () => {
        var A = registerDSL();
        const query = A.origin('New York NY');

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(1221);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(1221);
    });

    it("filter queries for source and destination in Airline DSL", async () => {
        var A = registerDSL();
        const query = A.origin('New York NY')
                        .destination('Los Angeles CA');

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(362);

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(362);
    });

    it("filter queries in Airline DSL with select", async () => {
        var A = registerDSL();
        const query = A.origin('New York NY')
                        .select('destination');

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(1221);
        expect(result[0].destination).to.equal("Los Angeles CA");

        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = await sqlExecutor.execute(query);

        expect(sqlResult).to.have.length(1221);
        expect(sqlResult[0].destination).to.equal("Los Angeles CA");
    });

    it("self join queries on source and destination", async () => {
        const query = Q.selfJoin("destination", "origin");
        const executor = new q.TypescriptExecutor(flights.slice(0, 100));
        var result = await executor.execute(query);

        expect(result).to.have.length(4712);
    });

    it("self join queries on source and destination and filter", async () => {
        const query = Q.selfJoin("destination", "origin")
                        .filter("origin_1", q.RelationalOperator.EqualTo, "New York NY")
                        .filter("destination_2", q.RelationalOperator.EqualTo, "Honolulu HI")
                        .select("origin_1", "destination_1", "destination_2", "flightId_1", "flightId_2");

        const executor = new q.TypescriptExecutor(flights.slice(0, 1000));
        var result = await executor.execute(query);

        expect(result).to.have.length(6696);
    });

    it("filter one stop flights using dsl", async () => {
        var A = registerDSL();
        const query = A.oneStop("New York NY", "Honolulu HI")

        const executor = new q.TypescriptExecutor(flights.slice(0, 1000));
        var result = await executor.execute(query);

        expect(result).to.have.length(6696);
    });

    it("distinct destinations from origin", async () => {
        var A = registerDSL();
        const query = A.origin("New York NY")
                        .select("destination")
                        .distinct();

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(8);
    });

    it("distinct cities operated by carrier", async () => {
        var A = registerDSL();
        const query = A.carrier("American Airlines Inc.")
                        .select("origin")
                        .distinct();

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(53);
    });

    it("flights on specific date", async () => {
        var A = registerDSL();
        const query = A.date(new Date("2005-07-05"));

        const executor = new q.TypescriptExecutor(flights);
        var result = await executor.execute(query);

        expect(result).to.have.length(305);
    });
});
