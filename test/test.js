"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const q = require("../src/q");
const flights_1 = require("./flights");
const Q = new q.IdNode();
// Register all the airline specific constructs. After doing this,
// call chaining should just work.
function registerDSL() {
    q.ASTNode.register('origin', function (city) {
        return this.filter("origin", q.RelationalOperator.EqualTo, city);
    });
    q.ASTNode.register('destination', function (city) {
        return this.filter("destination", q.RelationalOperator.EqualTo, city);
    });
    q.ASTNode.register('oneStop', function (origin, destination) {
        return this.selfJoin("destination", "origin")
            .filter("origin_1", q.RelationalOperator.EqualTo, origin)
            .filter("destination_2", q.RelationalOperator.EqualTo, destination);
    });
    q.ASTNode.register('carrier', function (carrier) {
        return this.filter("carrier", q.RelationalOperator.EqualTo, carrier);
    });
    q.ASTNode.register('date', function (date) {
        return this.filter("year", q.RelationalOperator.EqualTo, date.getUTCFullYear())
            .filter("day", q.RelationalOperator.EqualTo, date.getUTCDay());
    });
    return new q.IdNode();
}
describe("Tests", () => __awaiter(this, void 0, void 0, function* () {
    it("filter queries with equals", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY");
        var executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(1221);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(1221);
    }));
    it("filter queries with not equals", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("origin", q.RelationalOperator.NotEqualTo, "New York NY");
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(8779);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(8779);
    }));
    it("filter queries with greater than", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("departureDelay", q.RelationalOperator.GreaterThan, 5.0);
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(3236);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(3236);
    }));
    it("filter queries with greater than or equal to", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("departureDelay", q.RelationalOperator.GreaterThanOrEqualTo, 5.0);
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(3388);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(3388);
    }));
    it("filter queries with less than", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("departureDelay", q.RelationalOperator.LessThan, 5.0);
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(6612);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(6612);
    }));
    it("filter queries with less than or equal to", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("departureDelay", q.RelationalOperator.LessThanOrEqualTo, 5.0);
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(6764);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(6764);
    }));
    it("filter queries with select", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
            .select("destination");
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(1221);
        chai_1.expect(result[0].destination).to.equal("Los Angeles CA");
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(1221);
    }));
    it("filter queries with select multiple", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
            .select("destination", "carrier");
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(1221);
        chai_1.expect(result[0].destination).to.equal("Los Angeles CA");
        chai_1.expect(result[0].carrier).to.equal("American Airlines Inc.");
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(1221);
        chai_1.expect(sqlResult[0].destination).to.equal("Los Angeles CA");
        chai_1.expect(sqlResult[0].carrier).to.equal("American Airlines Inc.");
    }));
    it("filter queries for source in Airline DSL", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.origin('New York NY');
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(1221);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(1221);
    }));
    it("filter queries for source and destination in Airline DSL", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.origin('New York NY')
            .destination('Los Angeles CA');
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(362);
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(362);
    }));
    it("filter queries in Airline DSL with select", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.origin('New York NY')
            .select('destination');
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(1221);
        chai_1.expect(result[0].destination).to.equal("Los Angeles CA");
        var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");
        var sqlResult = yield sqlExecutor.execute(query);
        chai_1.expect(sqlResult).to.have.length(1221);
        chai_1.expect(sqlResult[0].destination).to.equal("Los Angeles CA");
    }));
    it("self join queries on source and destination", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.selfJoin("destination", "origin");
        const executor = new q.TypescriptExecutor(flights_1.flights.slice(0, 100));
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(4712);
    }));
    it("self join queries on source and destination and filter", () => __awaiter(this, void 0, void 0, function* () {
        const query = Q.selfJoin("destination", "origin")
            .filter("origin_1", q.RelationalOperator.EqualTo, "New York NY")
            .filter("destination_2", q.RelationalOperator.EqualTo, "Honolulu HI")
            .select("origin_1", "destination_1", "destination_2", "flightId_1", "flightId_2");
        const executor = new q.TypescriptExecutor(flights_1.flights.slice(0, 1000));
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(6696);
    }));
    it("filter one stop flights using dsl", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.oneStop("New York NY", "Honolulu HI");
        const executor = new q.TypescriptExecutor(flights_1.flights.slice(0, 1000));
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(6696);
    }));
    it("distinct destinations from origin", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.origin("New York NY")
            .select("destination")
            .distinct();
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(8);
    }));
    it("distinct cities operated by carrier", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.carrier("American Airlines Inc.")
            .select("origin")
            .distinct();
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(53);
    }));
    it("flights on specific date", () => __awaiter(this, void 0, void 0, function* () {
        var A = registerDSL();
        const query = A.date(new Date("2005-07-05"));
        const executor = new q.TypescriptExecutor(flights_1.flights);
        var result = yield executor.execute(query);
        chai_1.expect(result).to.have.length(305);
    }));
}));
//# sourceMappingURL=test.js.map