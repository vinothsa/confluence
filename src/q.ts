const mysql = require('mysql2/promise');
const bluebird = require('bluebird');

// Base class of all the nodes in the abstract syntax tree
export class ASTNode {
    type: string;

    // The list of new constructs registered. 
    static registeredFunctions = [];

    constructor(type: string) {
        this.type = type;

        // Whenever a new AST node is created, we have to inject the new constructs
        // for call chaining to work. 
        for (let f of ASTNode.registeredFunctions) {
            Object.defineProperty(this, f.name, { value: f.func });
        }
    }

    // Method to register new constructs. It is basically just method name and implementation
    static register(name: string, func: Function) {
        if (!ASTNode.registeredFunctions.find(item => item.name === name)) {
            ASTNode.registeredFunctions.push({ name, func });
        }
    }

    // Implements filters using relational operators
    filter(field: string, op: RelationalOperator, value: any): ASTNode {
        return new ThenNode(this, new FilterNode(field, op, value));
    }

    // Projects the given list of fields 
    select(...fields: string[]): ASTNode {
        return new ThenNode(this, new SelectNode(fields));
    }

    // Returns only distinct results
    distinct(): ASTNode {
        return new ThenNode(this, new DistinctNode());
    }

    // Implements a self-join based on the two given fields
    selfJoin(field1: string, field2: string): ASTNode {
        return new ThenNode(this, new SelfJoinNode(field1, field2));
    }
}

export class IdNode extends ASTNode {
    constructor() {
        super("Id");
    }
}

export class DistinctNode extends ASTNode {
    constructor() {
        super("Distinct");
    }
}

export enum RelationalOperator {
    EqualTo,
    NotEqualTo,
    GreaterThan,
    GreaterThanOrEqualTo,
    LessThan,
    LessThanOrEqualTo
}

export class FilterNode extends ASTNode {
    field: string;
    op: RelationalOperator;
    value: any;

    constructor(field: string, op: RelationalOperator, value: any) {
        super("Filter");
        this.field = field;
        this.op = op;
        this.value = value;
    }
}

export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }
}

export class SelectNode extends ASTNode {
    fields: string[];

    constructor(fields: string[]) {
        super("Select");
        this.fields = fields;
    }
}

export class SelfJoinNode extends ASTNode {
    field1: string;
    field2: string;

    constructor(field1: string, field2: string) {
        super("Join");
        this.field1 = field1;
        this.field2 = field2;
    }
}

// Base class for all executors. The query execution has been
// decoupled from the AST.
export class Executor {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    async execute(query: ASTNode): Promise<any> {
        throw new Error("Execute is not implemented");
    }
}

// Executor for in-memory Typescript collections. 
export class TypescriptExecutor extends Executor {
    data: any;

    constructor(data: any) {
        super("Typescript");
        this.data = data;
    }

    async execute(query: ASTNode): Promise<any> {
        return await this.executeImpl(query, this.data);
    }

    executeImpl(query: ASTNode, data: any) {
        if (query instanceof IdNode) {
            return data;
        } else if (query instanceof FilterNode) {
            return this.executeFilter(query, data);
        } else if (query instanceof ThenNode) {
            return this.executeImpl(query.second, this.executeImpl(query.first, data));
        } else if (query instanceof SelectNode) {
            return this.executeSelect(query, data);
        } else if (query instanceof SelfJoinNode) {
            return this.executeJoin(query, data);
        } else if (query instanceof DistinctNode) {
            return this.executeDistinct(data);
        }
    }

    executeDistinct(data: any) {
        var distinct = [];
        for (let d of data) {
            if (distinct.find(s => JSON.stringify(s) == JSON.stringify(d)) == null) {
                distinct.push(d);
            }
        }

        return distinct;
    }

    executeFilter(query: FilterNode, data: any) {
        var result = [];
        for (let d of data) {
            if (query.op === RelationalOperator.EqualTo && d[query.field] === query.value) {
                result.push(d);
            } else if (query.op === RelationalOperator.NotEqualTo && d[query.field] !== query.value) {
                result.push(d);
            } else if (query.op === RelationalOperator.GreaterThan && d[query.field] > query.value) {
                result.push(d);
            } else if (query.op === RelationalOperator.GreaterThanOrEqualTo && d[query.field] >= query.value) {
                result.push(d);
            } else if (query.op === RelationalOperator.LessThan && d[query.field] < query.value) {
                result.push(d);
            } else if (query.op === RelationalOperator.LessThanOrEqualTo && d[query.field] <= query.value) {
                result.push(d);
            }
        }

        return result;
    }

    executeSelect(query: SelectNode, data: any) {
        var result = [];
        for (let d of data) {
            var row = {};
            for (let f of query.fields) {
                row[f] = d[f];
            }

            result.push(row);
        }

        return result;
    }

    executeJoin(query: SelfJoinNode, data: any) {
        var dict1 = {};
        for (let d of data) {
            if (!dict1[d[query.field1]]) {
                dict1[d[query.field1]] = [ d ];
            } else {
                dict1[d[query.field1]].push(d);
            }
        }

        var dict2 = {};
        for (let d of data) {
            if (!dict2[d[query.field2]]) {
                dict2[d[query.field2]] = [ d ];
            } else {
                dict2[d[query.field2]].push(d);
            }
        }

        var result = [];
        for (let key in dict1) {
            if (dict2[key]) {
                for (let val1 of dict1[key]) {
                    for (let val2 of dict2[key]) {
                        var r = {};
                        for (let p in val1) {
                            r[p + "_1"] = val1[p];
                        }

                        for (let p in val2) {
                            r[p + "_2"] = val2[p];
                        }

                        result.push(r);
                    }
                }
            }
        }

        return result;
    }
}


// Executor for SQL databases. It walks the AST to create the SQL query
// and finally sends it to the database for execution and reads the result 
// back. I've ensured the results returned by both the executors are in 
// the same format. The same tests work on both. 
export class SqlExecutor extends Executor {
    host: string;
    user: string;
    password: string;
    database: string;
    table: string;

    constructor(host: string, user: string, password: string, database: string, table: string) {
        super("Hive");
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.table = table;
    }

    async execute(query: ASTNode) {
        var hq = new SqlQuery();
        this.executeImpl(query, hq);

        var select = "*";
        if (hq.project.length > 0) {
            select = hq.project.join(", ");
        }

        var q = "select " + select + " from " + this.table;
        if (hq.filters.length > 0) {
            q += " where ";
            q += hq.filters.join(" AND ");
        }

        return await this.queryImpl(q);
    }

    async queryImpl(query: string) {
        var config =
            {
                host: this.host,
                user: this.user,
                password: this.password,
                database: this.database,
                port: 3306,
                ssl: true,
                Promise: bluebird
            };

        const conn = await mysql.createConnection(config);
        const [rows, fields] = await conn.execute(query);
        conn.end();

        return rows;
    }

    executeImpl(query: ASTNode, hq: SqlQuery) {
        if (query instanceof IdNode) {
            return hq;
        } else if (query instanceof FilterNode) {
            return this.executeFilter(query, hq);
        } else if (query instanceof ThenNode) {
            this.executeImpl(query.first, hq);
            this.executeImpl(query.second, hq);
        } else if (query instanceof SelectNode) {
            return this.executeSelect(query, hq);
        }
    }

    executeFilter(query: FilterNode, hq: SqlQuery) {
        var val = "" + query.value;
        if (typeof query.value === "string") {
            val = "'" + query.value + "'";
        }

        if (query.op === RelationalOperator.EqualTo) {
            hq.filters.push(query.field + " = " + val);
        } else if (query.op === RelationalOperator.NotEqualTo) {
            hq.filters.push(query.field + " != " + val);
        } else if (query.op === RelationalOperator.GreaterThan) {
            hq.filters.push(query.field + " > " + val);
        } else if (query.op === RelationalOperator.GreaterThanOrEqualTo) {
            hq.filters.push(query.field + " >= " + val);
        } else if (query.op === RelationalOperator.LessThan) {
            hq.filters.push(query.field + " < " + val);
        } else if (query.op === RelationalOperator.LessThanOrEqualTo) {
            hq.filters.push(query.field + " <= " + val);
        }
    }

    executeSelect(query: SelectNode, hq: SqlQuery) {
        for (let f of query.fields) {
            hq.project.push(f);
        }
    }
}

export class SqlQuery {
    project = [];
    filters = [];
}