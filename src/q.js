"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require('mysql2/promise');
const bluebird = require('bluebird');
// Base class of all the nodes in the abstract syntax tree
class ASTNode {
    constructor(type) {
        this.type = type;
        // Whenever a new AST node is created, we have to inject the new constructs
        // for call chaining to work. 
        for (let f of ASTNode.registeredFunctions) {
            Object.defineProperty(this, f.name, { value: f.func });
        }
    }
    // Method to register new constructs. It is basically just method name and implementation
    static register(name, func) {
        if (!ASTNode.registeredFunctions.find(item => item.name === name)) {
            ASTNode.registeredFunctions.push({ name, func });
        }
    }
    // Implements filters using relational operators
    filter(field, op, value) {
        return new ThenNode(this, new FilterNode(field, op, value));
    }
    // Projects the given list of fields 
    select(...fields) {
        return new ThenNode(this, new SelectNode(fields));
    }
    // Returns only distinct results
    distinct() {
        return new ThenNode(this, new DistinctNode());
    }
    // Implements a self-join based on the two given fields
    selfJoin(field1, field2) {
        return new ThenNode(this, new SelfJoinNode(field1, field2));
    }
}
// The list of new constructs registered. 
ASTNode.registeredFunctions = [];
exports.ASTNode = ASTNode;
class IdNode extends ASTNode {
    constructor() {
        super("Id");
    }
}
exports.IdNode = IdNode;
class DistinctNode extends ASTNode {
    constructor() {
        super("Distinct");
    }
}
exports.DistinctNode = DistinctNode;
var RelationalOperator;
(function (RelationalOperator) {
    RelationalOperator[RelationalOperator["EqualTo"] = 0] = "EqualTo";
    RelationalOperator[RelationalOperator["NotEqualTo"] = 1] = "NotEqualTo";
    RelationalOperator[RelationalOperator["GreaterThan"] = 2] = "GreaterThan";
    RelationalOperator[RelationalOperator["GreaterThanOrEqualTo"] = 3] = "GreaterThanOrEqualTo";
    RelationalOperator[RelationalOperator["LessThan"] = 4] = "LessThan";
    RelationalOperator[RelationalOperator["LessThanOrEqualTo"] = 5] = "LessThanOrEqualTo";
})(RelationalOperator = exports.RelationalOperator || (exports.RelationalOperator = {}));
class FilterNode extends ASTNode {
    constructor(field, op, value) {
        super("Filter");
        this.field = field;
        this.op = op;
        this.value = value;
    }
}
exports.FilterNode = FilterNode;
class ThenNode extends ASTNode {
    constructor(first, second) {
        super("Then");
        this.first = first;
        this.second = second;
    }
}
exports.ThenNode = ThenNode;
class SelectNode extends ASTNode {
    constructor(fields) {
        super("Select");
        this.fields = fields;
    }
}
exports.SelectNode = SelectNode;
class SelfJoinNode extends ASTNode {
    constructor(field1, field2) {
        super("Join");
        this.field1 = field1;
        this.field2 = field2;
    }
}
exports.SelfJoinNode = SelfJoinNode;
// Base class for all executors. The query execution has been
// decoupled from the AST.
class Executor {
    constructor(name) {
        this.name = name;
    }
    execute(query) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error("Execute is not implemented");
        });
    }
}
exports.Executor = Executor;
// Executor for in-memory Typescript collections. 
class TypescriptExecutor extends Executor {
    constructor(data) {
        super("Typescript");
        this.data = data;
    }
    execute(query) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.executeImpl(query, this.data);
        });
    }
    executeImpl(query, data) {
        if (query instanceof IdNode) {
            return data;
        }
        else if (query instanceof FilterNode) {
            return this.executeFilter(query, data);
        }
        else if (query instanceof ThenNode) {
            return this.executeImpl(query.second, this.executeImpl(query.first, data));
        }
        else if (query instanceof SelectNode) {
            return this.executeSelect(query, data);
        }
        else if (query instanceof SelfJoinNode) {
            return this.executeJoin(query, data);
        }
        else if (query instanceof DistinctNode) {
            return this.executeDistinct(data);
        }
    }
    executeDistinct(data) {
        var distinct = [];
        for (let d of data) {
            if (distinct.find(s => JSON.stringify(s) == JSON.stringify(d)) == null) {
                distinct.push(d);
            }
        }
        return distinct;
    }
    executeFilter(query, data) {
        var result = [];
        for (let d of data) {
            if (query.op === RelationalOperator.EqualTo && d[query.field] === query.value) {
                result.push(d);
            }
            else if (query.op === RelationalOperator.NotEqualTo && d[query.field] !== query.value) {
                result.push(d);
            }
            else if (query.op === RelationalOperator.GreaterThan && d[query.field] > query.value) {
                result.push(d);
            }
            else if (query.op === RelationalOperator.GreaterThanOrEqualTo && d[query.field] >= query.value) {
                result.push(d);
            }
            else if (query.op === RelationalOperator.LessThan && d[query.field] < query.value) {
                result.push(d);
            }
            else if (query.op === RelationalOperator.LessThanOrEqualTo && d[query.field] <= query.value) {
                result.push(d);
            }
        }
        return result;
    }
    executeSelect(query, data) {
        var result = [];
        for (let d of data) {
            var row = {};
            for (let f of query.fields) {
                row[f] = d[f];
            }
            result.push(row);
        }
        return result;
    }
    executeJoin(query, data) {
        var dict1 = {};
        for (let d of data) {
            if (!dict1[d[query.field1]]) {
                dict1[d[query.field1]] = [d];
            }
            else {
                dict1[d[query.field1]].push(d);
            }
        }
        var dict2 = {};
        for (let d of data) {
            if (!dict2[d[query.field2]]) {
                dict2[d[query.field2]] = [d];
            }
            else {
                dict2[d[query.field2]].push(d);
            }
        }
        var result = [];
        for (let key in dict1) {
            if (dict2[key]) {
                for (let val1 of dict1[key]) {
                    for (let val2 of dict2[key]) {
                        var r = {};
                        for (let p in val1) {
                            r[p + "_1"] = val1[p];
                        }
                        for (let p in val2) {
                            r[p + "_2"] = val2[p];
                        }
                        result.push(r);
                    }
                }
            }
        }
        return result;
    }
}
exports.TypescriptExecutor = TypescriptExecutor;
// Executor for SQL databases. It walks the AST to create the SQL query
// and finally sends it to the database for execution and reads the result 
// back. I've ensured the results returned by both the executors are in 
// the same format. The same tests work on both. 
class SqlExecutor extends Executor {
    constructor(host, user, password, database, table) {
        super("Hive");
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.table = table;
    }
    execute(query) {
        return __awaiter(this, void 0, void 0, function* () {
            var hq = new SqlQuery();
            this.executeImpl(query, hq);
            var select = "*";
            if (hq.project.length > 0) {
                select = hq.project.join(", ");
            }
            var q = "select " + select + " from " + this.table;
            if (hq.filters.length > 0) {
                q += " where ";
                q += hq.filters.join(" AND ");
            }
            return yield this.queryImpl(q);
        });
    }
    queryImpl(query) {
        return __awaiter(this, void 0, void 0, function* () {
            var config = {
                host: this.host,
                user: this.user,
                password: this.password,
                database: this.database,
                port: 3306,
                ssl: true,
                Promise: bluebird
            };
            const conn = yield mysql.createConnection(config);
            const [rows, fields] = yield conn.execute(query);
            conn.end();
            return rows;
        });
    }
    executeImpl(query, hq) {
        if (query instanceof IdNode) {
            return hq;
        }
        else if (query instanceof FilterNode) {
            return this.executeFilter(query, hq);
        }
        else if (query instanceof ThenNode) {
            this.executeImpl(query.first, hq);
            this.executeImpl(query.second, hq);
        }
        else if (query instanceof SelectNode) {
            return this.executeSelect(query, hq);
        }
    }
    executeFilter(query, hq) {
        var val = "" + query.value;
        if (typeof query.value === "string") {
            val = "'" + query.value + "'";
        }
        if (query.op === RelationalOperator.EqualTo) {
            hq.filters.push(query.field + " = " + val);
        }
        else if (query.op === RelationalOperator.NotEqualTo) {
            hq.filters.push(query.field + " != " + val);
        }
        else if (query.op === RelationalOperator.GreaterThan) {
            hq.filters.push(query.field + " > " + val);
        }
        else if (query.op === RelationalOperator.GreaterThanOrEqualTo) {
            hq.filters.push(query.field + " >= " + val);
        }
        else if (query.op === RelationalOperator.LessThan) {
            hq.filters.push(query.field + " < " + val);
        }
        else if (query.op === RelationalOperator.LessThanOrEqualTo) {
            hq.filters.push(query.field + " <= " + val);
        }
    }
    executeSelect(query, hq) {
        for (let f of query.fields) {
            hq.project.push(f);
        }
    }
}
exports.SqlExecutor = SqlExecutor;
class SqlQuery {
    constructor() {
        this.project = [];
        this.filters = [];
    }
}
exports.SqlQuery = SqlQuery;
//# sourceMappingURL=q.js.map