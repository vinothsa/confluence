# confluence

Project report -
https://docs.google.com/document/d/1lQ3u4rxzHtcKeCwOEsExabTGQX4_svDfqUpy7jw1BHA/edit?usp=sharing

Screencast - 
https://drive.google.com/file/d/1EzJDAoHaUK16w4iU_kXvUiwLv_RkTXJZ/view  
(The trial software I used inserted a watermark right at the center! argh!! Sorry about that)

Design document -
https://docs.google.com/document/d/1ypq8PHnXuMmsI-8RrRRjjO3lZzN-5CIImH_pPKnTB18/edit?usp=sharing

Proposal - 
https://docs.google.com/document/d/1yPvB30MtlQk0f-MFGuMyHxxTtDAF3UcP0mIu2oZH5VQ/edit?usp=sharing

Pre-proposal -
https://docs.google.com/document/d/1y8M-P9a3z3jeEyl4CYsCrt7Ire8YaVlIUuAHbUCBY_g/edit?usp=sharing

Most data-oriented languages like SQL, LINQ, etc are too generic and complex queries can become an unreadable mess very quickly. 
So, constructs targeting the data at hand makes working with it a vastly better experience. Confluence is a language aimed at
bringing LINQ like functionality to Typescript with the flexibility of extending the language with new constructs specific to 
the domain. It is also modeled as a write-once, run anywhere framework, with this final project implementing queries on top of
in-memory JSON objects and SQL databases. I'm using the flight dataset from Bureau of Transportation Statistics. 

The project started off as an extension to HW2. Since the queries have to run at multiple stores and not just in-memory, the 
heavy use of lambdas was limiting. Typescript doesn't have rich language support like C# to inspect expressions. So I've re-implemented 
base functionality in HW2 without the use of lambdas as required. 

Examples of base functionality -   

1. Filters  
	Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY");
   	
2. Select   
	Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
                        .select("destination");
   
3. Distinct   
	Q.filter("origin", q.RelationalOperator.EqualTo, "New York NY")
                        .select("destination")
						.distinct();
   						
Some flight data specific constructs -  

1. Filter by origin   
	A.origin('New York NY')   
      .select('destination');   
	  
2. Filter by carrier   
	A.carrier("American Airlines Inc.")   
      .select("origin")
      .distinct();   
	  
3. Filter by date   
	A.date(new Date("2005-07-05"));   
	
4. One stop flights    
	A.oneStop("New York NY", "Honolulu HI")   
	
The examples above aren't the complete list and things can be mixed and matched for complex querying. There were
three main goals for the project -

1. Shallow embed the airline dsl over HW2 - achieved by not constructing the AST directly
2. No changes to the base code or functionality i.e. user should be able to plug-in new constructs. It is done like this 

	q.ASTNode.register('origin', function (city: string) {   
        return this.filter("origin", q.RelationalOperator.EqualTo, city);    
    });    
	
	and the usage is like this    
	
	A.origin('New York NY')    
      .select('destination');    
	  
3. Multiple provider support to run the query anywhere    
	const executor = new q.TypescriptExecutor(flights);    
    var result = await executor.execute(query);    

    var sqlExecutor = new q.SqlExecutor("vinothsdb.mysql.database.azure.com", "vinoths@vinothsdb", "Domain41", "dsl", "flights");    
    var sqlResult = await sqlExecutor.execute(query);    

There are tests covering all these aspects. 
